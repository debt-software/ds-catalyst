﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace level_editor
{
    public partial class Settings2 : Form
    {
        public Settings2()
        {
            InitializeComponent();
            textBox1.Text = Properties.Settings.Default.assetPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            assetChooser.ShowDialog();
            Properties.Settings.Default.assetPath = assetChooser.SelectedPath;
            textBox1.Text = Properties.Settings.Default.assetPath;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.assetPath = textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}
